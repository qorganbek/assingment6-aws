terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = var.aws_region
}

resource "aws_key_pair" "remote_connection" {
  key_name   = "remote_connection"
  public_key = file(var.path_pub_ssh)
}




resource "aws_instance" "server" {
  ami           = var.ami
  instance_type = var.instance-type
  key_name = aws_key_pair.remote_connection.public_key

  tags = {
    Name = "DevOpsFinal"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo adduser ansible",
      "sudo groupadd ansible",
      "sudo usermod -aG sudo ansible"
      "sudo apt-get update",
      "sudo apt-get install -y curl openssh-server ca-certificates",
      "sudo apt-get install -y postfix",
      "curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash",
      "sudo EXTERNAL_URL=\"http://${self.public_ip}\" apt-get install gitlab-ce",
    ]

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa")
      host        = self.public_ip
    }

  }

}
