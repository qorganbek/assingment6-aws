variable "aws_region" {
  description = "aws region"
  type = string
  default = "us-east-1"
}

variable "ami" {
  description = "ami"
  type = string
  default = "ami-0fc5d935ebf8bc3bc"
}

variable "instance-type" {
  description = "instance type"
  type = string
  default = "t2.micro"
}


variable "path_pub_ssh" {
  description = "Path my ssh public key"
  type = string
  default = "~/.ssh/id_rsa.pub"
}